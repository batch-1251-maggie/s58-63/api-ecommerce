const  Product = require("./../models/Product");
const User = require("./../models/User");


module.exports.getAllActive = () => {

	return Product.find({isActive:true}).then(result => {
		return result
	})
}

module.exports.addProduct = (reqBody) => {
	console.log(reqBody);
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then( (product, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
} 

module.exports.getAllProducts = () => {

	return Product.find().then(result => {
		return result
	})
}


module.exports.getSingleProduct = (params) => {

	return Product.findById(params.productId).then (product => {
		return product
	})
}


module.exports.editProduct = (params, reqBody) => {

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(params, updatedProduct, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}


module.exports.archiveProduct = (params) => {
	
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.unarchiveProduct = (params) => {
	
	let updatedProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}


module.exports.deleteProduct = (params) => {
	
	return Product.findByIdAndDelete(params).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.getAllProductsG = () => {

	return Product.find().then(result => {
		return result
	})
}

module.exports.getAllActiveG = () => {

	return Product.find({isActive:true}).then(result => {
		return result
	})
}

module.exports.getSingleProductG = (params) => {

	return Product.findById(params.productId).then (product => {
		return product
	})
}

module.exports.featureProduct = (params) => {
	
	let updatedProduct = {
		featured: true
	}

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.unfeatureProduct = (params) => {
	
	let updatedProduct = {
		featured: false
	}

	return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then ((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}