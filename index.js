const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 4000;
const app = express();
const cors = require('cors');

//routes
let userRoutes = require("./routes/userRoutes");
let productsRoutes = require("./routes/productRoutes");

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


//mongoose collection
mongoose.connect("mongodb+srv://Shadraque:201557Intp!@cluster0.lwdvy.mongodb.net/api_ecommerce?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}

).then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))


app.use("/api/users", userRoutes);
app.use("/api/products", productsRoutes);

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));