const express = require("express");
const router = express.Router();


const userController = require("./../controllers/userControllers");

const auth = require('./../auth');

//Check Email Exists
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(result => res.send(result));
});

//Register User
router.post("/register", (req, res)=> {
	userController.register(req.body).then(result => res.send(result));
})

//Login User
router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result))
})

//Get User Profile / Details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile(userData.id).then(result => res.send(result))
})


module.exports = router;
