const express = require("express");
const router = express.Router();
const auth = require('./../auth');

//controllers
const productController = require("./../controllers/productControllers");

//Get Active Products
router.get('/active', auth.verify, (req, res) => {

	productController.getAllActive().then(result => res.send(result));
});


//Get All Products
router.get("/all", auth.verify, (req, res) => {

	productController.getAllProducts().then(result => res.send(result));
})


//Add Product
router.post('/addProduct', auth.verify, (req, res) => {

	productController.addProduct(req.body).then(result => res.send(result));
});


//Get Single Product
router.get('/:productId', auth.verify, (req, res) => {

	productController.getSingleProduct(req.params).then(result => res.send(result));
});



//Edit Product
router.put('/:productId/edit', auth.verify, (req, res) => {

	productController.editProduct(req.params.productId, req.body).then(result => res.send(result));
});


//Archive Product
router.put('/:productId/archive', auth.verify, (req, res) => {

	productController.archiveProduct(req.params.productId, req.body).then(result => res.send(result));
});


//Unarchive Product
router.put('/:productId/unarchive', auth.verify, (req, res) => {

	productController.unarchiveProduct(req.params.productId, req.body).then(result => res.send(result));
});

//Delete Product
router.delete('/:productId/delete', auth.verify, (req, res) => {

	productController.deleteProduct(req.params.productId, req.body).then(result => res.send(result));
});

//Get All Products GUEST
router.get("/g/all", (req, res) => {

	productController.getAllProductsG().then(result => res.send(result));
})

//Get Active Products GUEST
router.get('/g/active', (req, res) => {

	productController.getAllActiveG().then(result => res.send(result));
});

//Get Single Product GUEST
router.get('/g/:productId', (req, res) => {

	productController.getSingleProductG(req.params).then(result => res.send(result));
});

//Archive Product
router.put('/:productId/feature', auth.verify, (req, res) => {

	productController.featureProduct(req.params.productId, req.body).then(result => res.send(result));
});


//Unarchive Product
router.put('/:productId/unfeature', auth.verify, (req, res) => {

	productController.unfeatureProduct(req.params.productId, req.body).then(result => res.send(result));
});


module.exports = router;